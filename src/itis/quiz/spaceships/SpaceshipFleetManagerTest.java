package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    static double points=0;
    ArrayList<Spaceship> shipList=new ArrayList<>();
    SpaceshipFleetManager main;
    public static void main(String[] args) {
        double totalPoints=0;
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        boolean test1 = spaceshipFleetManagerTest.GetShipByName_shipExists_taregetShip();
        if (test1){
            totalPoints=totalPoints+0.44;
            System.out.println("test 1 is ok");
        }else{
            System.out.println("test 1 failed");
        }
        boolean test2 = spaceshipFleetManagerTest.GetShipByName_shipExists_null();
        if (test2){
            totalPoints=totalPoints+0.44;
            System.out.println("test 2 is ok");
        }else{
            System.out.println("test 2 failed");
        }
        boolean test3 = spaceshipFleetManagerTest.getMostPowerfulShip_shipExists();
        if (test3){
            totalPoints=totalPoints+0.44;
            System.out.println("test 3 is ok");
        }else{
            System.out.println("test 3 failed");
        }
        boolean test4= spaceshipFleetManagerTest.getMostPowerfullShip_notExists_null();
        if (test4){
            totalPoints=totalPoints+0.44;
            System.out.println("test 4 is ok");
        }else{
            System.out.println("test 4 is failed");
        }
        boolean test5=spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipExists_cargoShipsList();
        if(test5){
            totalPoints=totalPoints+0.44;
            System.out.println("test 5 is ok");
        }else{
            System.out.println("test 5 failed");
        }
        boolean test6=spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipNotExists_null();
        if (test6){
            totalPoints=totalPoints+0.44;
            System.out.println("test 6 is ok");
        }else{
            System.out.println("test 6 failed");
        }
        boolean test7=spaceshipFleetManagerTest.getAllCivilianShips_shipNotExists_null();
        if (test7){
            totalPoints=totalPoints+0.44;
            System.out.println("test 7 is ok");
        }else{
            System.out.println("test 7 failed");
        }
        boolean test8=spaceshipFleetManagerTest.getAllCivilianShips_shipExists_civilianShips();
        if (test8){
            totalPoints=totalPoints+0.44;
            System.out.println("test 8 os ok");
        }else{
            System.out.println("test 8 failed");
        }
        boolean test9=spaceshipFleetManagerTest.getMostPowerfulShip_2shipExists_firstShip();
        if(test9){
            totalPoints=totalPoints+(0.44);
            System.out.println("test 9 is ok");
        }else{
            System.out.println("test 9 failed");
        }
points=totalPoints;
        System.out.println("total points: "+totalPoints);
    }
    private boolean GetShipByName_shipExists_taregetShip(){
        String testName="Lol";
        shipList.add(new Spaceship("Lol",100,100,1));
        shipList.add(new Spaceship("kek",100,100,1));
        shipList.add(new Spaceship("cheburek",100,100,1));
        Spaceship lol=main.getShipByName(shipList, testName);
        if (lol!=null && lol.getName().equals(testName)){
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }

    private boolean GetShipByName_shipExists_null(){
        String testName="helllloooooo";
        shipList.add(new Spaceship("Lol",100,100,1));
        shipList.add(new Spaceship("kek",100,100,1));
        shipList.add(new Spaceship("cheburek",100,100,1));
        Spaceship lol=main.getShipByName(shipList, testName);
        if (lol==null){
            shipList.clear();
            return true;

        }
        shipList.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipExists(){
        shipList.add(new Spaceship("Lol",10,100,1));
        shipList.add(new Spaceship("kek",20,100,1));
        shipList.add(new Spaceship("cheburek",40,100,1));
        Spaceship titan=main.getMostPowerfulShip(shipList);
        Spaceship ship=shipList.get(0);
        for (int i=0;i< shipList.size();i++){
            if (shipList.get(i).getFirePower()>ship.getFirePower()){
                ship=shipList.get(i);
            }
        }
        if (titan!=null && titan==ship){
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getMostPowerfullShip_notExists_null(){
        shipList.add(new Spaceship("Lol",0,100,1));
        shipList.add(new Spaceship("kek",0,100,1));
        shipList.add(new Spaceship("cheburek",0,100,1));
        Spaceship mars=main.getMostPowerfulShip(shipList);
        if (mars==null) {
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getAllShipsWithEnoughCargoSpace_shipExists_cargoShipsList(){
        int cargo=90;
        ArrayList<Spaceship> cargoShips;
        Spaceship lol=new Spaceship("Lol",0,0,0);
        Spaceship kek=new Spaceship("kek",100,100,0);
        Spaceship cheburek=new Spaceship("cheburek",0,100,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        cargoShips= main.getAllShipsWithEnoughCargoSpace(shipList,cargo);
        for (int i=0;i< cargoShips.size();i++){
            if (cargoShips.get(i).getCargoSpace()<cargo){
                shipList.clear();
                return false;
            }
        }

        shipList.clear();
        return true;
    }
    private boolean getAllShipsWithEnoughCargoSpace_shipNotExists_null(){
        int cargo=90;
        ArrayList<Spaceship> cargoTestShips;
        Spaceship lol=new Spaceship("Lol",0,0,0);
        Spaceship kek=new Spaceship("kek",70,10,0);
        Spaceship cheburek=new Spaceship("cheburek",0,1,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        cargoTestShips= main.getAllShipsWithEnoughCargoSpace(shipList,cargo);
        if (cargoTestShips==null) {
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getAllCivilianShips_shipNotExists_null(){
        ArrayList<Spaceship> civilianShips;
        Spaceship lol=new Spaceship("Lol",1,0,0);
        Spaceship kek=new Spaceship("kek",70,10,0);
        Spaceship cheburek=new Spaceship("cheburek",1,1,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        civilianShips= main.getAllCivilianShips(shipList);
        if (civilianShips==null) {
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getAllCivilianShips_shipExists_civilianShips(){
        ArrayList<Spaceship> civilianShips;
        Spaceship lol=new Spaceship("Lol",0,0,0);
        Spaceship kek=new Spaceship("kek",100,100,0);
        Spaceship cheburek=new Spaceship("cheburek",0,100,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        civilianShips= main.getAllCivilianShips(shipList);
        for (int i=0;i< civilianShips.size();i++){
            if (civilianShips.get(i).getFirePower()!=0){
                shipList.clear();
                return false;
            }
        }
        shipList.clear();
        return true;
    }
    private boolean getMostPowerfulShip_2shipExists_firstShip(){
        shipList.add(new Spaceship("Lol",10,100,1));
        shipList.add(new Spaceship("kek",40,100,1));
        shipList.add(new Spaceship("cheburek",40,100,1));
        Spaceship titan=main.getMostPowerfulShip(shipList);
        Spaceship ship=shipList.get(0);
        for (int i=0;i< shipList.size();i++){
            if (shipList.get(i).getFirePower()>ship.getFirePower()){
                ship=shipList.get(i);
            }
        }
        if (titan!=null && titan==ship){
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }


    public SpaceshipFleetManagerTest(SpaceshipFleetManager main) {
        this.main = main;
    }
}
