package itis.quiz.spaceships;

import java.util.ArrayList;
import org.junit.jupiter.api.*;

public class SpaceshippFleetManagerTestLibrary {
    ArrayList<Spaceship> shipList=new ArrayList<>();
    CommandCenter main;
@BeforeEach
public void doSmthFirst(){

    shipList.add(new Spaceship("Lol",100,100,1));
    shipList.add(new Spaceship("kek",100,100,1));
    shipList.add(new Spaceship("cheburek",100,100,1));
}
@AfterEach
public void getReady(){
    shipList.clear();
}

@Test
    public void GetShipByName_shipExists_null(){
        String testName="helllloooooo";
        Spaceship result=main.getShipByName(shipList,testName);
        Assertions.assertNull(result);

    }

    private boolean getMostPowerfulShip_shipExists(){
        shipList.add(new Spaceship("Lol",10,100,1));
        shipList.add(new Spaceship("kek",20,100,1));
        shipList.add(new Spaceship("cheburek",40,100,1));
        Spaceship titan=main.getMostPowerfulShip(shipList);
        Spaceship ship=shipList.get(0);
        for (int i=0;i< shipList.size();i++){
            if (shipList.get(i).getFirePower()>ship.getFirePower()){
                ship=shipList.get(i);
            }
        }
        if (titan!=null && titan==ship){
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getMostPowerfullShip_notExists_null(){
        shipList.add(new Spaceship("Lol",0,100,1));
        shipList.add(new Spaceship("kek",0,100,1));
        shipList.add(new Spaceship("cheburek",0,100,1));
        Spaceship mars=main.getMostPowerfulShip(shipList);
        if (mars==null) {
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getAllShipsWithEnoughCargoSpace_shipExists_cargoShipsList(){
        int cargo=90;
        ArrayList<Spaceship> cargoShips;
        Spaceship lol=new Spaceship("Lol",0,0,0);
        Spaceship kek=new Spaceship("kek",100,100,0);
        Spaceship cheburek=new Spaceship("cheburek",0,100,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        cargoShips= main.getAllShipsWithEnoughCargoSpace(shipList,cargo);
        for (int i=0;i< cargoShips.size();i++){
            if (cargoShips.get(i).getCargoSpace()<cargo){
                shipList.clear();
                return false;
            }
        }

        shipList.clear();
        return true;
    }
    private boolean getAllShipsWithEnoughCargoSpace_shipNotExists_null(){
        int cargo=90;
        ArrayList<Spaceship> cargoTestShips;
        Spaceship lol=new Spaceship("Lol",0,0,0);
        Spaceship kek=new Spaceship("kek",70,10,0);
        Spaceship cheburek=new Spaceship("cheburek",0,1,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        cargoTestShips= main.getAllShipsWithEnoughCargoSpace(shipList,cargo);
        if (cargoTestShips==null) {
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getAllCivilianShips_shipNotExists_null(){
        ArrayList<Spaceship> civilianShips;
        Spaceship lol=new Spaceship("Lol",1,0,0);
        Spaceship kek=new Spaceship("kek",70,10,0);
        Spaceship cheburek=new Spaceship("cheburek",1,1,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        civilianShips= main.getAllCivilianShips(shipList);
        if (civilianShips==null) {
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }
    private boolean getAllCivilianShips_shipExists_civilianShips(){
        ArrayList<Spaceship> civilianShips;
        Spaceship lol=new Spaceship("Lol",0,0,0);
        Spaceship kek=new Spaceship("kek",100,100,0);
        Spaceship cheburek=new Spaceship("cheburek",0,100,0);
        shipList.add(lol);
        shipList.add(kek);
        shipList.add(cheburek);
        civilianShips= main.getAllCivilianShips(shipList);
        for (int i=0;i< civilianShips.size();i++){
            if (civilianShips.get(i).getFirePower()!=0){
                shipList.clear();
                return false;
            }
        }
        shipList.clear();
        return true;
    }
    private boolean getMostPowerfulShip_2shipExists_firstShip(){
        shipList.add(new Spaceship("Lol",10,100,1));
        shipList.add(new Spaceship("kek",40,100,1));
        shipList.add(new Spaceship("cheburek",40,100,1));
        Spaceship titan=main.getMostPowerfulShip(shipList);
        Spaceship ship=shipList.get(0);
        for (int i=0;i< shipList.size();i++){
            if (shipList.get(i).getFirePower()>ship.getFirePower()){
                ship=shipList.get(i);
            }
        }
        if (titan!=null && titan==ship){
            shipList.clear();
            return true;
        }
        shipList.clear();
        return false;
    }



    public static void main(String[] args) {


    }
}
