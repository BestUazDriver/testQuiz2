package itis.quiz.spaceships;


import java.util.ArrayList;
import java.util.Scanner;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int maxFirePower = 0;
        Spaceship mostPowerfulShip = null;
        for(Spaceship spaceship: ships){
            if(spaceship.getFirePower()>maxFirePower && spaceship.getFirePower()!=0){
                maxFirePower = spaceship.getFirePower();
                mostPowerfulShip = spaceship;
            }
        }
        return mostPowerfulShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        Spaceship namedShip=null;
        for (int i=0;i<ships.size();i++){
            if(ships.get(i).getName()==name){
                namedShip=ships.get(i);
            }
        }
        return namedShip;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, int cargoSpace) {
        ArrayList<Spaceship> cargoShips=new ArrayList<>();
        for (int i=0;i<ships.size();i++){
            if (ships.get(i).getCargoSpace()>=cargoSpace){
                cargoShips.add(ships.get(i));
            }
        }
        if (cargoShips.size()==0){
            cargoShips=null;
        }
        return cargoShips;
    }




    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        for(Spaceship spaceship: ships){
            if(spaceship.getFirePower()<=0){
                civilianShips.add(spaceship);
            }
        }
        if (civilianShips.size()==0){
            civilianShips=null;
        }
        return civilianShips;
    }
}
