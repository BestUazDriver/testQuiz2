package itis.quiz;


/*
	Это класс-представление рациональной дроби. Вам необходимо реализовать все методы приведенные ниже, для работы с ним.
	Вы можете добавлять собственные служебные внутренние методы, но не можете менять или удалять существующие.
	Для вашего удобства конструктор, геттеры и сеттеры уже есть.
	!МЕНЯТЬ СИГНАТУРЫ И ТИПЫ ВОЗВРАЩАЕМЫХ ЗНАЧЕНИЙ НЕЛЬЗЯ!
 */
public class RationalFraction {

	private int numerator;

	private int denominator;

	public RationalFraction(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}

	//сокращает дробь, насколько это возможно
	void reduce() {
		boolean NumNegative = numerator < 0;
		boolean DenNegative = denominator < 0;
		if (NumNegative) numerator *= -1;
		if (DenNegative) denominator *= -1;
		int divisor = Math.min(this.numerator, this.denominator);
		while (divisor > 1) {
			if (this.numerator % divisor == 0 & this.denominator % divisor == 0) {
				this.numerator /= divisor;
				this.denominator /= divisor;
				divisor = Math.min(this.numerator, this.denominator);
			}
			divisor--;
		}
		if (NumNegative^DenNegative) this.numerator *= -1;
	}

	//сравнивает с другой дробью (не забудьте про сокращение!)
	public boolean equals(RationalFraction otherFraction) {
		return value()==otherFraction.value();
	}

	//Текстовое представление дроби
	@Override
	public String toString() {
		return numerator+"/"+denominator;
	}

	//перевод в десятичную дробь
	public Double toDecimalFraction() {
		return value();
	}

	//выделение целой части
	public Integer getNumberPart() {
		return numerator / denominator;
	}

	//сложение с другой дробью (не забудьте про сокращение здесь и во всех остальных математических операциях)
	public RationalFraction add(RationalFraction otherFraction) {
		reduce();
		otherFraction.reduce();
		int addedFractionNumerator = this.getNumerator() * otherFraction.getDenominator()
				+ otherFraction.getNumerator() * this.getDenominator();
		int addedFractionDenominator = this.getDenominator() * otherFraction.getDenominator();
		RationalFraction addedFraction = new RationalFraction(addedFractionNumerator, addedFractionDenominator);
		addedFraction.reduce();
		return addedFraction;
	}

	//вычитание другой дроби
	public RationalFraction sub(RationalFraction otherFraction) {
		reduce();
		otherFraction.reduce();
		return add(new RationalFraction(otherFraction.getNumerator() * -1, otherFraction.getDenominator()));
	}

	//умножение на другую дробь
	public RationalFraction multiply(RationalFraction otherFraction) {

		RationalFraction newFraction = new RationalFraction(this.numerator * otherFraction.getNumerator(), this.denominator * otherFraction.getDenominator());
		newFraction.reduce();
		return newFraction;
	}

	//деление на другую дробь
	public RationalFraction divide(RationalFraction otherFraction) {
		return multiply(otherFraction.fractionInverse());
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}

	//дополнительный метод
	public RationalFraction fractionInverse(){
		return new RationalFraction(getDenominator(), getNumerator());
	}
	public double value() {
		try {
			return (double) this.numerator / this.denominator;
		}catch(ArithmeticException e){
			System.out.println(e.toString());
			return this.numerator > 0 == this.denominator > 0 ?Double.POSITIVE_INFINITY:Double.NEGATIVE_INFINITY;
		}
	}
}
